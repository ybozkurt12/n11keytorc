# Keytorc için n11 testi

## Test için kullanılan teknolojiler : 
1.  NodeJS https://nodejs.org/en/
2.  Selenium kütüphanesi https://www.seleniumhq.org/download/
3.  WebdriveIO kütüphanesi http://webdriver.io/
4.  JUnit kütüphanesi https://junit.org/junit5/

## Log ve Klasör Bilgileri
* Test de hata alınması durumunda hata fotoğraflar "./errorShots" klasörüne atılmaktadır
* Testin her adımında fotoğraf alınmaktadır bu fotoğraflar "./screenshot" klasörüne atılmaktadır
* Testin her adımı .xml formatında loglanmaktadır log site adıyla otomatik olarak oluşturulmaktadır bu log "./resultant" klasörüne atılmaktadır.
* Test dosyası "./test/n11test.js" şeklinde bulunmaktadır.

## Teste Dair Bilgiler
* Herşey otomatik oluşturulmaktadır. (log, fotoğraflama ve test).
* Test unit olarak bağımsız bir şekilde yazılmıştır.
* Test anının videosu çekilmiştir bu da proje dosyasının içerisinde "./test.MOV" olarak kaydedilmiştir.
* Editor olarak vscode kullanılmıştır.
* Teste dair configuration bilgileri "./wdio.conf.js" dosyasında yer almaktadır.

### Tester : Yunus Emre BOZKURT
### Test yapılma tarihi : 10/04/18 Salı 