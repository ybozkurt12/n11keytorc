describe('N11', function () {
    it('n11 keytorc testi', function () {
        //n11 sitesi açıldı
        browser.url('/');
        browser.saveScreenshot('./screenshot/Photo01_n11_sitesi_açıldı.png');
        browser.pause('3000');

        //Giriş yap butonuna tıklandı
        browser.click('#header > div > div > div.hMedMenu > div.customMenu > div.myAccountHolder.customMenuItem > div > div > a.btnSignIn');
        browser.pause('5000');

        //Üyelik bilgileri girildi ve üye giriş yapıldı
        let result = browser.execute(function () {
            $('#email').val('ybozkurt12@gmail.com')
            $('#password').val('Yunus8256926')
            $('#loginButton').click()
        });
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo02_n11_login_bilgileri_girildi.png');
        browser.pause('5000');

        //Popup kapatıldı
        let isExisting;
        isExisting = browser.isExisting('html body div.seg-popup.seg-top-center.segFadeInUp.fancybox-segmentify.fancybox-segmentify-notification');
        if (isExisting) {
            browser.click('html body div.seg-popup.seg-top-center.segFadeInUp.fancybox-segmentify.fancybox-segmentify-notification div.fancybox-skin div.fancybox-outer div.fancybox-inner div.fancybox-body div.sgm-notification-buttons button.sgm-notification-close');
            browser.pause('5000');  
            browser.saveScreenshot('./screenshot/Photo03_n11_popup_kapatıldı.png');
            browser.pause('5000');    
        }

        //Arama bölüme samsung yazıldı
        browser.setValue('#searchData', 'Samsung');
        browser.pause('5000');  
        browser.saveScreenshot('./screenshot/Photo04_n11_samsung_yazıldı.png');
        browser.pause('5000');

        //Ara butonuna tıklandı
        browser.click('#header > div > div > div.hMedMenu > div.searchBox > a');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo05_n11_samsung_arama_sonuçları.png');
        browser.pause('5000');

        //2.sayfa butonuna tıklandı
        browser.click('#contentListing > div > div > div.productArea > div.pagination > a:nth-child(2)');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo06_n11_samsung_arama_2.Sayfa.png');
        browser.pause('5000');

        //Favoriler ekle butonuna tıklandı
        browser.click('html body div#wrapper.wrapper div#contentListing.content div.container div.listingHolder div.productArea section.group.listingGroup.resultListGroup div#view.listView ul li.column div#p-241436198.columnContent div.proDetail span.textImg.followBtn');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo07_n11_favorilerime_ekle.png');
        browser.pause('5000');

        //Hesabım butonuna tıklandı
        browser.click('#header > div > div > div.hMedMenu > div.customMenu > div.myAccountHolder.customMenuItem.hasMenu > div.myAccount > a.menuTitle');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo08_n11_hesabım.png');
        browser.pause('5000');

        //Favorilerim sayfasına tıklandı
        browser.click('#myAccount > div.accMenu > div.accMenu-cover > div.accNav > ul > li:nth-child(6) > a');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo09_n11_favorilerim_sayfası.png');
        browser.pause('5000');

        //Favoriler detay sayfasına tıklandı
        browser.click('#myAccount > div.accContent > ul > li.wishGroupListItem.favorites > div > a > h4');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo10_n11_favoriler_sayfası_detay.png');
        browser.pause('5000');

        //Sil butonuna tıklandı
        browser.click(' html body div#wrapper.wrapper.myPage div.content div.container div#myAccount.clearfix div.accContent div.group.listingGroup.wishListGroup div#view.listView ul li.column.wishListColumn div#p-241436198.columnContent div.wishProBtns span.deleteProFromFavorites');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo11_n11_favori_ürün_sil_butonuna_tıklandı.png');
        browser.pause('5000');

        //Tamam butonuna tıklandı
        browser.click('body > div.lightBox > div > div > span');
        browser.pause('5000');
        browser.saveScreenshot('./screenshot/Photo12_n11_favori_ürün_sil_onaylama_ve_bitiş.png');
        browser.pause('5000');
    });
});